Biological Data Science
========================================================
author: M Hallett
date: March 2019
autosize:true
font-family: 'Helvetica' 
#output: beamer-presentation 

## Module 2, Lecture 1: 
## RNA-seq gene expression,  descriptive statistics, visualization


Exploring a breast cancer gene expression dataset
========================================================
* This is the TCGA gene expression data for breast cancer.
* [https://cancergenome.nih.gov/]([https://cancergenome.nih.gov/](https://cancergenome.nih.gov/) The Cancer Genome Atlass<br>


```{r}
load("~/repos/T4LS19/data/tcga.RDATA")
names( tcga )
```

A three part data structure that combines clinical and gene expression data
========================================================
Action item: Draw on paper what the tcga datastructure looks like.
```{r}
dim(tcga$clinical)
dim(tcga$probe.info)
dim(tcga$exprs)
```


RNA-sequencing measures transcript levels
========================================================
```{r}
tcga$probe.info[1:5,]
tcga$probe.info[42:45,]
```


The corresponding gene expression measurements (1st 10 patients)
========================================================

```{r}
tcga$exprs[1:10, 1:5]
```
Histopathological and clinical data
========================================================
Draw on paper what the tcga datastructure looks like.

```{r}
tcga$clinical[1:2, 1:6 ]
names(tcga$clinical)
```

Histopathological variable: Estrogen Receptor from IHC measurements
========================================================
Here "er"" is a clinical variable. It is measured using immunohistochemistry (IHC) of ESR1 protein expression.

```{r}
attach(tcga)
er.pos.pats <- subset(clinical, er) 
er.neg.pats <- subset(clinical, !er)
length(er.pos.pats[,1]); length(er.neg.pats[,1])
```

Patients are ER+ or ER-
========================================================
```{r}
er.pos.pats$id
er.neg.pats$id
```

Closer examinatin of one ER+ patient
========================================================
```{r}
er.pos.pats[1,1:6]
er.pos.pats[1,7:14]
er.pos.pats[1,15:20]
er.pos.pats[1, 21:24]
er.pos.pats[1, 25:27]
```

ESR1 is the Estrogen Receptor gene
========================================================
There are several transcripts that measure ESR1 mRNA levels.
```{r}
esr1 <- which(probe.info$gene.name == "ESR1")
probe.info[esr1,]
```

ESR1 expression
========================================================
Here are the observed ESR1 levels.
```{r}
exprs[esr1,1:5]
```

A comparison of protein vs transcript levels
========================================================
*So let's compare immunohistochemistry (IHC) levels (proteins) against transcript levels (mRNA)
```{r}
exprs[esr1,er.pos.pats$id[1:4]]
exprs[esr1,er.neg.pats$id[1:4]]
```



Differential Expression
========================================================
- Parametric statistical tests: the t-test
- Alternative Hypothesis $H_1$: $\mu_{good} - \mu_{bad} \neq 0$</code>.
- Null Hypothesis $H_0$:  $\mu_{good} - \mu_{bad} = 0$.
```{r}
t.test(exprs[esr1,er.pos.pats$id], exprs[esr1,er.neg.pats$id[1:4]], var.equal=TRUE) # two sample t-test
```
So it appears that ESR1 expression at the gene level is significantly 
different between patients that are positive or negative for  ER at the protein level (p-value < 0.01).
(Note that we pooled all transcripts here.)




Exploration via Visualization
========================================================
- Sometimes statistical tests and estimates such as p-values are a bit "opaque"
- Important to explore data visually.
- There are many built in functions in R for visualization data.
- The simplest is perhaps the hist() and plot() functions
- They are limited in power: ggplot2 is more powerful


A classic R plot() function 
========================================================

```{r fig.height = 10, fig.width = 10, fig.align="center"}
plot(exprs[esr1[1], ], exprs[esr1[2],], data = clinical, xlab = "mRNA Probe 1", ylab = "mRNA Probe 2")
```

A classic R plot() function 
========================================================

```{r fig.height = 10, fig.width = 10, fig.align="center"}
plot(exprs[esr1[1], ], exprs[esr1[3],], data = clinical, xlab = "mRNA Probe 1", ylab = "mRNA Probe 2")
```

A classic R plot() function 
========================================================

```{r fig.height = 10, fig.width = 10, fig.align="center"}
plot(exprs[esr1[1], ], exprs[esr1[6],], data = clinical, xlab = "mRNA Probe 1", ylab = "mRNA Probe 2")
```


Classic R plot() function 
========================================================
```{r fig.height = 10, fig.width = 10, fig.align="center"}
df <- as.data.frame(t(exprs[esr1,]))
plot(df, df)
```

So let's use probe 1 from here on in.


Visualizaton with ggplot2
========================================================
* There are some basic plotting functions in R but they are limited.
* The ggplot2 package is  powerful.
* [http://ggplot2.org/resources/2007-vanderbilt.pdf]([http://ggplot2.org/resources/2007-vanderbilt.pdf](http://ggplot2.org/resources/2007-vanderbilt.pdf) ggplot2 tutorial from the creator<br>
* 
```{r}
#install.packages("ggplot2")   # get it from CRAN
library(ggplot2)
```


We explore our RNA-seq data in ggplot2
========================================================
* In particular, we'll look at the the expression of ESR1 in relation
to different clinical variables.
* We need to do a bit of work to get our data.frame ready.
* In particular, for qplot() we need to put the expression data into the clinical data.frame

```{r}
dim(clinical)
clinical$esr1.1 <- exprs[esr1[1],]   # let's just use probes 1 and 4 which we found to be good
clinical$esr1.3 <- exprs[esr1[3],]   # let's just use probes 1 and 4 which we found to be good

clinical[1:5, 26:29]
```




qplot, a simple way to work with ggplot2
========================================================
* qplot wraps up all the details of ggplot with a
familiar syntax borrowed from plot
* Additional features:<br>
• Automatically scales data<br>
• Can produce any type of plot<br>
• Facetting and margins<br>
• Creates objects that can be saved and modified

quickplot == qplot (Two different transcripts)
========================================================
.

```{r fig.height = 7, fig.width = 7, fig.align="center"}
qplot(esr1.1, esr1.3, data = clinical, xlab = "mRNA Probe 1", ylab = "mRNA Probe 3") 

```

quickplot  (Smoothing)
========================================================
.

```{r fig.height = 7, fig.width = 7, fig.align="center"}
qplot(esr1.1, esr1.3, data = clinical, xlab = "mRNA Probe 1", ylab = "mRNA Probe 3",
      geom = c("point", "smooth")) 

```

quickplot  (Discrete Coloring)
========================================================
.

```{r fig.height = 7, fig.width = 7, fig.align="center"}
qplot(esr1.1, esr1.3, data = clinical, xlab = "mRNA Probe 1", ylab = "mRNA Probe 3",
      geom = c("point", "smooth"), color = er) 

```

quickplot  (Continuous Coloring)
========================================================
.

```{r fig.height = 7, fig.width = 7, fig.align="center"}
qplot(esr1.1, age, data = clinical, xlab = "mRNA Probe 1", ylab = "Age",
      geom = c("point", "smooth"), color = age) 

```
quickplot  (Revisited: Is age related to ER expressing tumors?)
========================================================
.

```{r fig.height = 7, fig.width = 7, fig.align="center"}
qplot(esr1.1, age, data = clinical, xlab = "mRNA Probe 1", ylab = "Age",
      geom = c("point", "smooth"), color = er) 

```
quickplot  (Revisited: Is age related to ER expressing tumors?)
========================================================
.

```{r fig.height = 7, fig.width = 7, fig.align="center"}
qplot(esr1.1, age, data = clinical, xlab = "mRNA Probe 1", ylab = "Age",
      geom = c("point", "smooth"), color = er, shape = factor(stage)) 

```

quickplot  (Boxplots)
========================================================
.

```{r fig.height = 7, fig.width = 7, fig.align="center"}
qplot(x=1, y=esr1.1, data = clinical, xlab = "mRNA Probe 1",
      geom = "boxplot")  

```

quickplot  (Boxplots)
========================================================
.

```{r fig.height = 7, fig.width = 7, fig.align="center"}
qplot(x=event, y=esr1.1, data = clinical, xlab = "Recurrence?",
      geom = c("boxplot"))  

```

Does not look like much of a relationship but ...

quickplot  (Violinplots - the violin killed the box)
========================================================
.

```{r fig.height = 7, fig.width = 7, fig.align="center"}
qplot(x=event, y=esr1.1, data = clinical, xlab = "Recurrence?",
      geom = c("violin"))  

```
Maybe? 

quickplot  (Violinplots - the violin killed the box)
========================================================
.

```{r fig.height = 7, fig.width = 7, fig.align="center"}
qplot(x=event, y=esr1.1, data = clinical, xlab = "Recurrence?",
      geom = c("violin", "jitter"))  

```

quickplot  (Violinplots - the violin killed the box)
========================================================
.

```{r fig.height = 7, fig.width = 7, fig.align="center"}
qplot(x=event, y=esr1.1, data = clinical, xlab = "Recurrence?",
      geom = c("violin", "jitter"),
      color = factor(stage))  
```



quickplot  - compare with HER2
========================================================
.

```{r fig.height = 7, fig.width = 7, fig.align="center"}
her2.probes <- which(probe.info$gene.name=="ERBB2")
dfh <- as.data.frame(t(exprs[her2.probes,]))
plot(dfh, dfh)

# Ok probe 1 it is.
clinical$her2e <- exprs[her2.probes[1],]   

```

quickplot  - compare with HER2
========================================================
.

```{r fig.height = 7, fig.width = 7, fig.align="center"}
qplot( her2e, data = clinical, color = factor(stage), geom="histogram" )
```

quickplot  - compare with HER2
========================================================
.

```{r fig.height = 7, fig.width = 7, fig.align="center"}
qplot( esr1.1, her2e, data = clinical, color = factor(stage) )
```







heatmaps 
========================================================
.

```{r}
# install.packages("gplots")  # different than ggplot2
library(gplots)

```



heatmaps 
========================================================
.

```{r}
# we are going to plot the expression of the most variable genes.
# first we compute the expression of each gene
evar <- apply(exprs,MARGIN=1,var)
evar[1:5]; length(evar)
mostVariable <- exprs[evar>quantile(evar,0.999),]
dim(mostVariable)
mostVariable[1:5, 1:5]


# so this makes a new matrix that restricted to only  the top .1% of probes.
```
heatmaps 
========================================================
.
We mean center this data to make it more comparable
```{r}

mcl.mostVar <- t(scale(t(mostVariable)))
```

heatmaps 
========================================================
.

```{r}
heatmap.2(mcl.mostVar,trace="none",col=greenred(10))

```


heatmaps 
========================================================

* Ok we explore heatmaps a bit.
* [https://github.com/LeahBriscoe/AdvancedHeatmapTutorial]([https://github.com/LeahBriscoe/AdvancedHeatmapTutorial](https://github.com/LeahBriscoe/AdvancedHeatmapTutorial) A nice tutorial from Leah Briscoe<br>
* [https://www.youtube.com/watch?v=T7_j444LMZs]([https://www.youtube.com/watch?v=T7_j444LMZs](https://www.youtube.com/watch?v=T7_j444LMZs) The YouTube accompanying video<br>> 


```{r}
# git clone https://github.com/LeahBriscoe/AdvancedHeatmapTutorial.git
```






BIOL480 (c) M Hallett, CB-Concordia
========================================================
![CB-Concordia](MyFigs/sysbiologo.png)

